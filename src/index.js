// console.log(require('dotenv').config());
// import envify from 'envify';
// console.log(envify);
import dotenv from 'dotenv';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.css';
import 'jquery/dist/jquery.min.js'
import 'bootstrap/dist/js/bootstrap.min.js'
import './index.css';
import Api from './Api/index';
import Auth from './Auth/index';

window.api = new Api();
window.auth = new Auth();

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
