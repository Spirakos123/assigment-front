import React, { Component } from 'react';
import axios from 'axios';

class Home extends Component {

    state = {
        currentDate: ''
    };

    componentDidMount() {
        this.getCurrentDate();
    }

    async getCurrentDate() {
        // let axiosInstance = axios.create({
        //     baseURL: 'https://assigment-back.herokuapp.com/api'
        // });
        let res = await window.api.call('get',process.env.REACT_APP_API_URL + '/get-date');
        let currentDate = res.data.data;
        this.setState( { currentDate } );
    };

    render() {
        return (
            <div>
                <h2>Current Date: {this.state.currentDate}</h2>
            </div>
        );
    }
}

export default Home;
