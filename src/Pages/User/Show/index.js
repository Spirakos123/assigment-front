import React , {Component} from 'react';
import {connect} from 'react-redux';
import {fetchUsers} from "../../../actions/Users";

class ShowUsersList extends Component {

    componentWillMount() {
        this.props.fetchUsers();
    }

    render() {
        let userItems;
        if(this.props.users.length > 0) {
            userItems = this.props.users.map(user => (
                <div key={user.email}>
                    <h3>{user.name}</h3>
                    <h3>{user.surname}</h3>
                    <h3>{user.email}</h3>
                    <hr />
                </div>

            ));
        }else {
            userItems = <h2 className="text-center">No users registered yet.</h2>
        }

        return (
            <div>
                <h1 className="text-center">Users</h1>
                {userItems}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    users: state.users.items
});

export default connect(mapStateToProps, { fetchUsers })(ShowUsersList);