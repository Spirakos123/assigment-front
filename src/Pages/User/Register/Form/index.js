import React , {Component} from 'react';
import {connect} from 'react-redux';
import {saveUser} from "../../../../actions/Users";

const initialState = {
    isLoading: false,
    userName: "",
    userSurname: "",
    userEmail: "",
    userPassword: "",
    userPasswordConfirm: "",
    saveDone: false
};

class UserRegisterForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            userName: '',
            userSurname: '',
            userEmail: '',
            userPassword: '',
            userPasswordConfirm: '',
            saveDone: false,
            errorMessage: '',
            passwordErrorMessage: ''
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.msg && nextProps.msg === 'Success') {
            this.setState({ saveDone: true });
            setTimeout( () => {
                this.setState(initialState);
            },1300 )
        }
    }

    handleChange = event => {
        const inputId = event.target.id;
        const inputValue = event.target.value;

        this.setState({
            [inputId]: inputValue
        });
    };

    handleFormSubmit = async event => {
        event.preventDefault();

        this.setState({ isLoading: true });

        if (!this.stateDataIsEmpty()) {
            const userData = {
                name: this.state.userName,
                surname: this.state.userSurname,
                email: this.state.userEmail,
                password: this.state.userPassword
            };

            if (this.state.userPassword === this.state.userPasswordConfirm) {
                this.props.saveUser(userData);
            } else {
                this.setErrorMessage('Passwords mismatch');
            }
        }

    };

    setErrorMessage = (message) => {
        this.setState({errorMessage: message});
    };

    stateDataIsEmpty = () => {
        if ( !(this.state.userName &&
            this.state.userSurname &&
            this.state.userEmail &&
            this.state.userPassword)
        ) {
            this.setErrorMessage('All fields is required');
            return true;
        }

        this.setErrorMessage('');

        return false;
    };

    checkPasswordLength = (event) => {
        const inputId = event.target.id;
        const inputValue = event.target.value;

        if (inputValue.length < 6 || inputValue.length > 10) {
            this.setState({ passwordErrorMessage: 'Password mismatch from requirements' });
        } else {
            this.setState({ passwordErrorMessage: '' })
        }

        this.setState({
            [inputId]: inputValue
        });
    };

    renderPasswordErrorMessage = () => {
        return this.state.passwordErrorMessage ?
            <div className="alert alert-danger">{this.state.passwordErrorMessage}</div>
            : '';
    };

    renderSaveIsDoneMessage = () => {
        return this.state.saveDone ?
            <div className="alert alert-success">Saved</div>
        : '';
    };

    renderErrorMessages = () => {
        return this.state.errorMessage ?
            <div className="alert alert-danger">{this.state.errorMessage}</div>
        : '';
    };

    renderSaveButton = () => {
        const loadingSavingButton = <button className="btn btn-success" type="button" disabled>
                <span className="spinner-border spinner-border-sm" role="status"
                      aria-hidden="true"></span>
            Saving...
        </button>;

        const savingButton = <button type="submit" className="btn btn-success">Sign up</button>;

        return this.state.isLoading ? loadingSavingButton : savingButton;
    };

    render() {
        return (
            <React.Fragment>
                <form onSubmit={this.handleFormSubmit}>
                    <div className="form-group row">
                        <label htmlFor="userName" className="col-sm-3 col-form-label">Name</label>
                        <div className="col-sm-9">
                            <input type="text"
                                   className="form-control"
                                   id="userName"
                                   placeholder="Name"
                                   value={this.state.userName}
                                   onChange={this.handleChange.bind(this)}
                                   required
                            />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="userSurname" className="col-sm-3 col-form-label">Surname</label>
                        <div className="col-sm-9">
                            <input type="text"
                                   className="form-control"
                                   id="userSurname"
                                   placeholder="Surname"
                                   value={this.state.userSurname}
                                   onChange={this.handleChange}
                                   required
                            />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="userEmail" className="col-sm-3 col-form-label">Email</label>
                        <div className="col-sm-9">
                            <input type="email"
                                   className="form-control"
                                   id="userEmail"
                                   placeholder="Email"
                                   value={this.state.userEmail}
                                   onChange={this.handleChange}
                                   required
                            />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="userPassword" className="col-sm-3 col-form-label">Password</label>
                        <div className="col-sm-9">
                            <input type="password"
                                   className="form-control"
                                   id="userPassword"
                                   placeholder="Password"
                                   value={this.state.userPassword}
                                   onChange={this.checkPasswordLength}
                                   required
                            />
                            <small id="passwordHelpBlock"
                                   className="form-text text-muted">
                                Your password must be 6-10 characters long.
                            </small>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="userPasswordConfirm" className="col-sm-3 col-form-label">Confirm Password</label>
                        <div className="col-sm-9">
                            <input type="password"
                                   className="form-control"
                                   id="userPasswordConfirm"
                                   placeholder="Confirm password"
                                   value={this.state.userPasswordConfirm}
                                   onChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <div className="form-group row">
                        <div className="col-sm-12 text-center">
                            {this.renderSaveButton()}
                        </div>
                    </div>
                </form>
                {this.renderPasswordErrorMessage()}
                {this.renderErrorMessages()}
                {this.renderSaveIsDoneMessage()}
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
    msg: state.users.msg
});

export default connect(mapStateToProps, { saveUser })(UserRegisterForm);