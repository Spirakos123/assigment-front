import axios from 'axios';

class Api {
    constructor () {
        // this.auth = window.auth;
    }

    call (requestType, url, data = null) {
        return new Promise((resolve, reject) => {
            if (window.auth.tokenExists()) {
                // console.log('token exists')
                axios[requestType](url, data)
                    .then(response => {
                        resolve(response);
                    })
                    .catch(({response}) => {
                        if (response.status === 401) {
                            window.auth.getToken().then( token => {
                                window.auth.setToken(token);
                                axios[requestType](url, data)
                                    .then(response => {
                                        resolve(response);
                                    })
                                    .catch(({response}) => {
                                        if (response.status === 401) {
                                            // auth.getToken();
                                        }

                                        reject(response);
                                    });
                            });
                        }

                        reject(response);
                    });
            } else {
                // console.log('token not exists')
                window.auth.removeTokenFromHeaders();
                window.auth.getToken().then( token => {
                    window.auth.setToken(token);
                    axios[requestType](url, data)
                        .then(response => {
                            resolve(response);
                        })
                        .catch(({response}) => {
                            if (response.status === 401) {
                                // auth.getToken();
                            }

                            reject(response);
                        });
                });
            }
        });
    }

    // sendUrl (requestType, url, data = null) {
    //     axios[requestType](url, data)
    //     .then(response => {
    //         resolve(response);
    //     })
    //     .catch(({response}) => {f
    //         if (response.status === 401) {
    //             // auth.getToken();
    //         }
    //
    //         reject(response);
    //     });
    // }
}

export default Api;