import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import './App.css';
import { Provider } from "react-redux";
import store from './store/configureStore';

import Header from './Components/Header/index';
import MainBodyContent from './Components/MainBodyContent/index';

class App extends Component {
  render() {
    return (
        <Provider store={store}>
            <Router>
              <Header />
              <MainBodyContent />
            </Router>
        </Provider>
    );
  }
}

export default App;
