import { FETCH_USERS, SAVE_USER } from "../actionTypes";
import axios from "axios";

export const saveUser = userData => dispatch => {
    axios.post( process.env.REACT_APP_API_URL + '/user/register',
       { body: userData }
    )
    .then(res => dispatch({
       type: SAVE_USER,
       payload: res.data
    }));
};

export const fetchUsers = () => dispatch => {
    // console.log(axios.defaults.headers.common)
    window.api.call( 'get',process.env.REACT_APP_API_URL + '/get-users')
    .then(res => dispatch({
        type: FETCH_USERS,
        payload: res.data
    }));
};