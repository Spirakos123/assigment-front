import {FETCH_USERS, SAVE_USER} from "../../actions/actionTypes";

const initialState = {
    items: [],
    msg: {},
};

export default function (state = initialState, action) {
    switch (action.type) {
        case SAVE_USER:
            return {
                ...state,
                msg: action.payload.msg
            };
        case FETCH_USERS:
            return {
                ...state,
                items: action.payload.users
            };
        default:
            return state;
    }
}