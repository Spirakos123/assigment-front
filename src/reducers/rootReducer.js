import {combineReducers} from "redux";
import usersReducers from './Users/index';

export default combineReducers({
    users: usersReducers
});