import axios from 'axios';
import Cookies from 'js-cookie';
class Auth {
    constructor() {
        if(!this.tokenExists()) {
            this.removeTokenFromHeaders();
            this.getToken().then( token => {
                this.setToken(token);
            });
        }else {
            const token = Cookies.get('token');
            // axios.defaults.headers.common = {'Authorization': `Bearer ${token}`};
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
        }
    }

    async getToken ()
    {
        let res = await axios.post(process.env.REACT_APP_API_URL + '/token');
        return res.data;
    }

    tokenExists ()
    {
        if (!Cookies.get('token')) {
            return false;
        }

        return true;
    }

    headerAuthCheck () {
        if (!axios.defaults.headers.common.Authorization) {
            return false;
        }

        return true;
    }

    setToken (token)
    {
        if (token) {
            const fiveMinutes = new Date(new Date().getTime() + 5 * 60 * 1000);
            Cookies.set('token', token, {
                expires: fiveMinutes
            });
            // axios.defaults.headers.common = {'Authorization': `Bearer ${token}`};
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
        }
    }

    removeTokenFromHeaders ()
    {
        delete  axios.defaults.headers.common['Authorization'];
    }

}

export default Auth;