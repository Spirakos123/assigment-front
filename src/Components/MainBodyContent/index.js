import React, {Component} from 'react';
import { Route } from 'react-router-dom';
import Home from '../../Pages/Home/index';
import UserRegisterForm from '../../Pages/User/Register/Form/index';
import ShowUsersList from '../../Pages/User/Show/index'

class MainBodyContent extends Component {

    render() {
        return (
            <div className="container py-4">
                <Route exact path="/" component={Home} />
                <Route exact path="/user/register/form" component={UserRegisterForm} />
                <Route exact path="/show/users" component={ShowUsersList} />
            </div>
        );
    }
}

export default MainBodyContent;